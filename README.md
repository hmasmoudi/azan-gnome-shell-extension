### Description

Azan is an Islamic prayer times extension for Gnome Shell. This is a fork of  [Fahrinh's Azan GNOME shell extension](https://github.com/fahrinh/azan-gnome-shell-extension).

### Features

- List 5 prayer times
- Optionally display Imsak, Sunrise, Sunset, Midnight
- Show remaining time for the upcoming prayer
- Show current date in Hijri calendar
- Display a notification when it's time for prayer
- Automatic location detection
- Display times in either 24 hour or 12 hour format
- Adjustement of Hijri date

### Installation

1. Download zip file : https://gitlab.com/hmasmoudi/azan-gnome-shell-extension
2. Extract to azan-gnome-shell-extension-master
3. make install

### Changelog

- 01 : initial upload with a reworked version with multiple fixes
- 02 : update CI and repository url
- 03 : Add support for Hijri adhustement

### License

Licensed under the GNU General Public License, version 3

### Third-Party Assets & Components

- [PrayTimes.js](http://praytimes.org/manual/)
- [HijriCalendar-Kuwaiti.js](http://www.al-habib.info/islamic-calendar/hijricalendar-kuwaiti.js)

